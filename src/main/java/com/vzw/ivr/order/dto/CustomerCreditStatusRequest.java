/**
 * @author Brijesh Kumar
 *
 * @date 12-Mar-2021
 */
package com.vzw.ivr.order.dto;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@JsonPropertyOrder({
	"callId",
	"mdn",
	"dnis",
	"locationCode",
	"creditApplicationNum",
	"serviceAreaZip"
	
})
@Setter
@Getter
@ToString
@NoArgsConstructor
public class CustomerCreditStatusRequest implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String callId;
	private String mdn;
	private String dnis;
	private String locationCode;
	private String creditApplicationNum;
	private String serviceAreaZip;
	

}
